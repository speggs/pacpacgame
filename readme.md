# PacPac Game Team 1

This game is built on the Unity Engine and will most likely be fully built on the engine as writing it in 
Android Studio is like trying to build a car without a frame. Using Unity is like building a car with everything 
except you just have to put it together

## How to use / install
Download / clone / fork the repo and have the latest version of Unity installed. Go into Unity and click 
File -> Open Project -> FolderYouInstalledTo
Once the project is open, open the Main scene under the scenes folder. This will allow you to see the current 
version of the game (which is bare bones right now)

gitignore is setup and make sure to enable Visible Meta Files in the editor by going to 
Edit -> Project Settings -> Editor

When that is all set and done, you should have something similar to this
![Editor Img](https://i.imgur.com/463G20S.png)