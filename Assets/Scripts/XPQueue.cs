﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XPQueue : MonoBehaviour {

    public Text xpText;
    public int queueid = 0;
    public bool isTaken = false;

    private string xpMsg = "Enemy Defeated [+{0}]";
    private string levelMsg = "Level Up! [{0}]";
    private bool attemptedSit = false;
    private bool didSit = false;
    private float sitTime = 0;
    public Color xpColor;
    public Color levelColor;

    private static Queue<Text> queue = new Queue<Text>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (queue.Count >= 1 &&  queue.Peek() == xpText && xpText.transform.localPosition.y > 0) {
            queue.Dequeue();
        }

        if (queue.Count >= 1 && queue.Peek() == xpText && !xpText.enabled) {
            xpText.enabled = true;
        }

        if (!xpText.enabled) { return;  }

        if (xpText.enabled && transform.localPosition.y < 225) {

            float y = xpText.transform.localPosition.y;

            if (y >= 0 && !didSit && sitTime < Time.realtimeSinceStartup && attemptedSit) {
                didSit = true;
            }
            else if (!didSit && y >= 0 && sitTime < Time.realtimeSinceStartup) {
                sitTime = Time.realtimeSinceStartup + 1;
                attemptedSit = true;
            }
            else if ((didSit && y >= 0) || y < 0) {

                int multi = 400;

                if (y < 0) {
                    multi = 200;
                }

                float newY = y + (Time.smoothDeltaTime * multi);

                if (newY > 0) {
                    Color currentC = xpText.color;
                    xpText.color = new Color(currentC.r, currentC.g, currentC.b, (255 - newY) / (float)255);
                }

                xpText.transform.localPosition = new Vector3(0, newY, 0);
                if (newY >= 225) {
                    xpText.enabled = false;
                    isTaken = false;
                }
            }
        }else if (xpText.enabled && transform.localPosition.y >= 225) {
            xpText.enabled = false;
            isTaken = false;
        }
    }

    private void SetupNew() {
        attemptedSit = false;
        didSit = false;
        transform.localPosition = new Vector2(0, -300);
        queue.Enqueue(xpText);
    }

    public void SetXPMessage(int amount) {
        xpText.text = string.Format(xpMsg, amount);
        xpText.color = xpColor;
        SetupNew();
    }
    
    public void SetLevelMessage(int level) {
        xpText.text = string.Format(levelMsg, level);
        xpText.color = levelColor;
        //print("We got level up msg");
        SetupNew();
    }
}
