﻿using UnityEngine;
using System.Collections;
using System.Timers;
using System;
using System.Collections.Generic;

[RequireComponent(typeof(Transform))]
//[RequireComponent (typeof(Moves))]

public class Creature : MonoBehaviour {

    protected string cName = "Unkown";
    protected int level = 1;
    protected Moves.Types elementType = Moves.Types.NATURE;
    protected int health = 400;
    protected int maxhealth = 400;
    protected int defenseMod = 0;
    protected int attackMod = 0;
    private bool isBeingDamaged = false;
    private bool isFinishedDmg = false;
    private bool isDead = false;
    protected Dictionary<string, int> moveTbl = new Dictionary<string, int>();
    private SpriteRenderer spriteRender;
    private AudioSource audioSource;
    private bool isSpawning = false;
    private Vector3 spritePos;

    private Transform tr;
    private Vector2 orgPos;
    private float damageForce = 1;

    public virtual void Start() {
        Moves.Start();
        tr = this.GetComponent<Transform>();
        orgPos = tr.transform.position;
        spriteRender = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
        spritePos = spriteRender.transform.localPosition;
    }

    void FixedUpdate() {
        if (isSpawning) {
            if (isBeingDamaged) {
                isBeingDamaged = false;
            }
            spriteRender.transform.localPosition = Vector3.Lerp(spriteRender.transform.localPosition, spritePos, Time.deltaTime * 5);
            if (Vector3.Distance(spriteRender.transform.localPosition,spritePos) < .01) {
                isSpawning = false;
            }
        }
        else if (isBeingDamaged) {
                tr.transform.position = orgPos + new Vector2(UnityEngine.Random.Range(-damageForce, damageForce), UnityEngine.Random.Range(-damageForce, damageForce));
                spriteRender.color = new Color(1f, 1f, 1f, Mathf.PingPong(Time.time * 5, 1));
        }else if (isFinishedDmg) {
            isFinishedDmg = false;
            spriteRender.color = new Color(1f, 1f, 1f, 1f);
            tr.transform.position = orgPos;
        }
    }

    private Color GenNewColor(int r, int g, int b) {
        float div = 255f;
        return new Color(r / div, g / div, b / div);
    }

    public virtual void SetNewCreature(string name, int maxhealth, int health, int level, Moves.Types type) {
        this.cName = name;
        this.health = health;
        this.maxhealth = maxhealth;
        this.level = level;
        this.elementType = type;
        isDead = false;

        spriteRender.sprite = Resources.Load<Sprite>("Players/"+MonsterGenerator.GetResourceByType(type));
        if (this is Player) {
            spriteRender.flipX = true;
            spriteRender.transform.localPosition = spriteRender.transform.localPosition - new Vector3(1.5f, 0, 0);
        }else {
            spriteRender.transform.localPosition = spriteRender.transform.localPosition + new Vector3(1.5f, 0, 0);
        }

        isSpawning = true;

        onCreatureInitialized();
    }

    protected virtual void Attack(string move, Creature whoToAttack) { // dont know how to make this much cleaner
        if (isDead) {
            GameManager.manager.onNewEnemyAttack();
            return;
        }
        Hashtable attackInfo = Moves.GetAttackInfo(move);

        if (!attackInfo.ContainsKey("attackType") || (Moves.AttackTypes)attackInfo["attackType"] == Moves.AttackTypes.NONE) {
            print("Error with move " + move);
            return;
        }

        Creature whoToTarget = whoToAttack;
        Moves.AttackTypes attackType = (Moves.AttackTypes)attackInfo["attackType"];
        Moves.AttackTypes subAttackType = (Moves.AttackTypes)attackInfo["extraAttackType"];

        bool hasSecondAttack = (subAttackType != Moves.AttackTypes.NONE);

        bool isPrimaryFriendly = Moves.isMoveFriendly(attackType);
        bool isPrimaryDefense = Moves.isMoveDefense(attackType);
        bool isPrimaryAttack = Moves.isMoveAttack(attackType);
        bool isPrimaryDmg = Moves.isMoveDmg(attackType);

        if (isPrimaryFriendly) {
            whoToTarget = this;
        }

        int primaryDmg = UnityEngine.Random.Range((int)attackInfo["minVal"], (int)attackInfo["minVal"]);
        int secondaryDmg = 0;

        GameManager.manager.onCreatureAttacked(this, whoToTarget, move, attackType,primaryDmg);

        if (Moves.shouldInverse(attackType)) {
            primaryDmg = -primaryDmg;
        }

        if (isPrimaryDefense) {
            DoModifyDefense(whoToTarget, primaryDmg);
        } else if (isPrimaryAttack) {
            DoModifyAttack(whoToTarget, primaryDmg);
        } else if (isPrimaryDmg) {
            DoDmgAttack(whoToTarget, primaryDmg);
        }

        if (hasSecondAttack) {

            bool isSecondaryDefense = Moves.isMoveDefense(subAttackType);
            bool isSecondaryAttack = Moves.isMoveAttack(subAttackType);
            bool isSecondaryDmg = Moves.isMoveDmg(subAttackType);

            if ((bool)attackInfo["extraHitsPlayer"]) {
                whoToTarget = this;
            }
            else {
                whoToTarget = whoToAttack;
            }

            secondaryDmg = UnityEngine.Random.Range((int)attackInfo["extraMin"], (int)attackInfo["extraMax"]);

            if (Moves.shouldInverse(subAttackType)) {
                secondaryDmg = -secondaryDmg;
            }

            if (isSecondaryDefense) {
                DoModifyDefense(whoToTarget, secondaryDmg);
            }
            else if (isSecondaryAttack) {
                DoModifyAttack(whoToTarget, secondaryDmg);
            }
            else if (isSecondaryDmg) {
                DoDmgAttack(whoToTarget, secondaryDmg);
            }

            if (moveTbl.ContainsKey(move)) {
                moveTbl[move]--;
            }
        }

        if (this is Monster) {
            GameManager.manager.onEnemyAttacked(move, primaryDmg);
        }
        else if(this is Player) {
            GameManager.manager.onPlayerAttacked(move, primaryDmg);
        }
    }

    public virtual void onCreatureKilled() {
        isDead = true;
        GameManager.manager.onCreatureKilled(this);

        //sr.color = GenNewColor(UnityEngine.Random.Range(0, 255), UnityEngine.Random.Range(0, 255), UnityEngine.Random.Range(0, 255));
    }

    public virtual int onTakeDamage(int damage) {
        if (tr != null) {

            health -= damage;
			//health = 0;

            if (health <= 0) {
                onCreatureKilled();
                return 0;
            }
            else {
                GameManager.manager.onCreatureTookDamage(this, damage, health);
            }
            return health;
        }
        print("No sprite render valid");
        return 0;
    }

    public virtual void DMGAnim(int dmg) {
        isBeingDamaged = true;
        damageForce = GetDamageForce(dmg);

        int t = (int)(damageForce * 8000);

        Timer time = new Timer(t);

        time.Elapsed += (sender, elasped) => {
            this.FinishDmg();
            time.Stop();
        };
        time.Start();
    }

    public void DMGModAnim(bool isIncrease) {
        ParticleSystem sys = GetComponent<ParticleSystem>();
        var main = sys.main;

        main.startSize = 5;
        var sysRender = GetComponent<ParticleSystemRenderer>();
        sysRender.material = Resources.Load<Material>("Images/sword_mat");

        if (isIncrease) {
            main.startRotation = 0;
            main.startColor = new Color(.05f, .8f, .05f);
        }
        else {
            main.startRotation = 180;
            main.startColor = new Color(.8f, .05f, .05f);
        }

        sys.Play();
    }

    public void DefModAnim(bool isIncrease) {
        ParticleSystem sys = GetComponent<ParticleSystem>();
        var main = sys.main;

        main.startSize = 3;
        var sysRender = GetComponent<ParticleSystemRenderer>();
        sysRender.material = Resources.Load<Material>("Images/shield_mat");

        if (isIncrease) {
            main.startRotation = 0;
            main.startColor = new Color(.05f, .8f, .05f);
        }
        else {
            main.startRotation = 0;
            main.startColor = new Color(.8f, .05f, .05f);
        }

        sys.Play();
    }

    public void HealthAnim() {
        ParticleSystem sys = GetComponent<ParticleSystem>();
        var main = sys.main;

        main.startSize = 2;
        var sysRender = GetComponent<ParticleSystemRenderer>();
        sysRender.material = Resources.Load<Material>("Images/health_mat");

        main.startRotation = 0;
        main.startColor = new Color(1f, 1f, 1f);
       
        sys.Play();
    }
	
	public int GetLevel() {
        return level;
    }

    public int GetHealth() {
        return health;
    }

	// public int GetMaxHealth() {
        // return maxhealth;
    // }
    
	// public int GetDefMod() {
        // return defenseMod;
    // }
	 
	// public int GetAtkMod() {
        // return attackMod;
    // }
	 
    private void FinishDmg() {
        isBeingDamaged = false;
        isFinishedDmg = true;
    }

    protected float GetDamageForce(int damage) {
        return (float)damage / 80;
    }

    protected virtual void onCreatureInitialized() {
        moveTbl = Moves.GenerateMovesByType(elementType);
    }

    public void EmitSound(string path) {
        AudioClip clip = (AudioClip) Resources.Load("Sounds/"+path);
        audioSource.PlayOneShot(clip);
    }

    // Attack funcs

    public void DoModifyDefense(Creature target, int mod) {
        target.defenseMod = Mathf.Clamp(target.defenseMod + mod, -20, 20);
        GameManager.manager.onCreatureDefenseChanged(target, mod);
    }

    public void DoModifyAttack(Creature target, int mod) {
        target.attackMod = Mathf.Clamp(target.attackMod + mod, -200, 200);
        GameManager.manager.onCreatureAttackChanged(target, mod);
    }

    public void DoDmgAttack(Creature target,int dmg) {
        float newDmg = (float)((GetLevel() * .8) * dmg);
        dmg = Mathf.CeilToInt(newDmg);
        target.onTakeDamage(dmg);
    }

    public string GetName() {
        return cName;
    }

    public int GetHealthByLevel(int level) {
        return level * 45;
    }

}
