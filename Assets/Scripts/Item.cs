﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public string itemName { get; set; }
    public int itemId { get; set; }
    public string itemDescription { get; set; }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
   
	}

    public virtual void OnUse()
    {
        print("This was used");
    }
}
