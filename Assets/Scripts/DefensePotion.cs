﻿using UnityEngine;
//using UnityEditor;

public class DefensePotion : Item
{


    public override void OnUse()
    {
        Player player = GameManager.manager.player;
        float newDef = (float)((player.GetLevel() * .8) * 2);
        int def = Mathf.CeilToInt(newDef);
        player.DoModifyDefense(player, def);
    }
}