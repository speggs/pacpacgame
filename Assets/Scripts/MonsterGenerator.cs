﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MonsterGenerator : MonoBehaviour {
    private static string[] word1 = { "Acidic", "Active", "Aged", "Agile", "Agitated", "Ancient", "Angry", "Anguished", "Arctic", "Arid", "Aromatic", "Awful", "Barren", "Bewitched", "Big Bad", "Bitter", "Black", "Bleak", "Blind", "Blissful", "Blue", "Bold", "Broken", "Bronze", "Brown", "Bruised", "Calm", "Canine", "Cloudy", "Cold", "Colossal", "Corrupt", "Crazy", "Creepy", "Cruel", "Dangerous", "Dark", "Dead", "Deadly", "Defiant", "Delirious", "Dirty", "Disfigured", "Disgusting", "Dismal", "Dreary", "Electric", "Empty", "Enraged", "Eternal", "Evil", "Faint", "False", "Feline", "Fickle", "Filthy", "Forsaken", "Giant", "Gray", "Greedy", "Grim", "Gross", "Grotesque", "Gruesome", "Grumpy", "Hairy", "Half", "Haunting", "Hidden", "Hollow", "Horrible", "Hungry", "Icy", "Infamous", "Insane", "Insidious", "Jagged", "Lanky", "Lean", "Living", "Lone", "Lonely", "Mad", "Meager", "Mean", "Monstrous", "Muted", "Nasty", "Needy", "Noxious", "Outlandish", "Parallel", "Putrid", "Quick", "Quiet", "Reckless", "Rotten", "Shady", "Sick", "Skeletal", "Tall", "Thin", "Twin", "Ugly", "Undead", "Vengeful", "Volatile", "White", "Wild", "Wretched" };
    private static string[] word2 = { "Man", "Woman", "Child", "Mutant", "Abomination", "Glob", "Monster", "Behemoth", "Beast", "Freak", "Horror", "Fiend", "Abnormality", "Brute", "Miscreation", "Monstrosity", "Savage", "Deformity", "Deviation", "Anomaly", "Weirdo", "Abortion", "Malformation", "Blob", "Lump", "Bulge", "Tumor", "Creature", "Critter", "Vermin", "Being", "Thing", "Revenant", "Keeper", "Guardian", "Witch", "Troglodyte", "Charmer", "Vine", "Tree", "Plant", "Howler", "Statue", "Vision", "Dweller", "Lich", "Pest", "Gnoll", "Ooze", "Hag", "Hunter", "Entity", "Phenomenon", "Body", "Figure", "Presence", "Corpse", "Demon", "Wraith", "Herder", "Mongrel", "Hybrid", "Mutt", "Teeth", "Eyes", "Face", "Screamer", "Howler", "Shrieker", "Wailer", "Babbler", "Mumbler", "Creeper" };
    private static string[] word3 = { "Abyss", "Acid", "Ash", "Aura", "Bane", "Barb", "Blade", "Blaze", "Blight", "Bone", "Boulder", "Bowel", "Brine", "Cave", "Cavern", "Chaos", "Cinder", "Cloud", "Coffin", "Corpse", "Crypt", "Curse", "Dawn", "Decay", "Doom", "Dread", "Dream", "Dusk", "Dust", "Ember", "Fetid", "Flame", "Fog", "Foul", "Fright", "Frost", "Gall", "Gas", "Germ", "Gloom", "Glow", "Grave", "Grieve", "Grime", "Gut", "Haunt", "Hell", "Hollow", "Horror", "Infernal", "Inferno", "Metal", "Mist", "Mold", "Morn", "Mourn", "Murk", "Nether", "Night", "Phantom", "Phase", "Plague", "Poison", "Putrid", "Razor", "Rot", "Rotting", "Rust", "Shade", "Shadow", "Slag", "Smog", "Smoke", "Soil", "Sorrow", "Soul", "Spectral", "Spirit", "Spite", "Steam", "Stench", "Stink", "Stone", "Taint", "Tangle", "Terror", "Thorn", "Thunder", "Tomb", "Toxin", "Trance", "Umbra", "Vamp", "Vapor", "Venom", "Vex", "Vile", "Voodoo", "Vortex", "Warp", "Web", "Wisp" };
    private static string[] word4 = { "Agile", "Amphibian", "Aquatic", "Arctic", "Barb-Tailed", "Black-Eyed", "Black-Striped", "Blind", "Blood-Eyed", "Bloodthirsty", "Bright", "Brutal", "Burnt", "Chaotic", "Cobalt", "Cold-Blooded", "Crazed", "Crimson", "Crowned", "Dark", "Diabolical", "Ebon", "Electric", "Elusive", "Evasive", "Feathered", "Feral", "Fiery", "Furry", "Giant", "Glacial", "Golden", "Greater", "Grim", "Grisly", "Hidden", "Horned", "Howling", "Iron", "Iron-Scaled", "Ivory", "Jade", "Lone", "Long-Horned", "Mad", "Malevolent", "Masked", "Matriarch", "Monstrous", "Obsidian", "Onyx", "Painted", "Patriarch", "Primeval", "Primitive", "Rabid", "Raging", "Ravaging", "Red-Eyed", "Ruthless", "Sapphire", "Savage", "Scarred", "Screeching", "Silver", "Silver-Striped", "Slender", "Stalking", "Stormcloud", "Supreme", "Taloned", "Tattooed", "Titanic", "Titanium", "Tusked", "Vicious", "White-Eyed", "Wild" };
    private static List<string[]> wordList = new List<string[]> { word1, word2, word3, word4 };

    private static Dictionary<string, Moves.Types> ourMonsters = new Dictionary<string, Moves.Types>() {
        {"Creature1",Moves.Types.WATER},
        {"Creature2",Moves.Types.FIRE},
        {"Creature3",Moves.Types.ELECTRIC},
        {"Creature4",Moves.Types.NATURE},
    };
    private static Dictionary<Moves.Types, string> HashedTypes = new Dictionary<Moves.Types, string>();

    private const int amountOfCreatures = 4;
    private static bool wasLoaded = false;
    private static System.Random random = new System.Random();

    void Start() {
    }

    // Generates a random creature name from the lists above
    public static string GenerateMonsterName() {
        int howManyWords = UnityEngine.Random.Range(1, 3);
        string ourName = "";

        for (int k = 0; k < howManyWords; k++) {
            string[] listnames = wordList[random.Next(wordList.Count)];
            ourName += " "+listnames[random.Next(listnames.Length)];
        }
        return ourName;
    }

    // Generates the resource name and the type for that monster
    public static Dictionary<string,Moves.Types> GenerateCreatureDetails() {

        Dictionary<string, Moves.Types> ourDetails = new Dictionary<string, Moves.Types>();
        List<string> pathList = new List<string>(ourMonsters.Keys);

        string ourKey = pathList[new System.Random().Next(pathList.Count)];

        ourDetails.Add(ourKey, ourMonsters[ourKey]);

        return ourDetails;
    }

    public static string GetResourceByType(Moves.Types type) {
        
        if (!wasLoaded) {
            List<string> keys = new List<string>(ourMonsters.Keys);

            foreach (string key in keys) {
                Moves.Types loadType = ourMonsters[key];
                HashedTypes[loadType] = key;
            }
            wasLoaded = true;
        }

        return HashedTypes[type];
    }

}
