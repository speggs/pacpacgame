﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour {

    // This class is dead for now since unity events are dumb and slow

    private static Dictionary<string, UnityEvent> hooks = new Dictionary<string, UnityEvent>();

    public static void Add(string name, UnityAction func){

        UnityEvent ourEvent = null;

        if (hooks.TryGetValue(name, out ourEvent)) {
            ourEvent.AddListener(func);
        }
        else {
            ourEvent = new UnityEvent();
            hooks.Add(name, ourEvent); // Doesn't exist
            ourEvent.AddListener(func);
        }
    }

    public static void Remove(string name, UnityAction func) {

        UnityEvent ourEvent = null;
    
        if (hooks.TryGetValue(name, out ourEvent)) {
            ourEvent.RemoveListener(func);
        }
    }

    public static void Call(string name, params object[] args) {
        UnityEvent ourEvent = null;
        if (hooks.TryGetValue(name, out ourEvent)) {
           // ourEvent.Invoke(args);
        }
    }
}
