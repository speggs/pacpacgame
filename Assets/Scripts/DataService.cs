﻿using SQLite4Unity3d;
using UnityEngine;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

public class DataService  {

	public static SQLiteConnection _connection;

	public DataService(string DatabaseName){

#if UNITY_EDITOR
            var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID 
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		
#elif UNITY_STANDALONE_OSX
		var loadDb = Application.dataPath + "/Resources/Data/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif
            _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        Debug.Log("Final PATH: " + dbPath);     

	}

	public static void CreateDB(SQLiteConnection conn){
		conn.CreateTable<DBPlayer> ();
		conn.CreateTable<DBMonster>();
		conn.CreateTable<DBInventory>();
	}
	
	public static int insertIntoPlayers(string pPlayername, SQLiteConnection conn){
		var p = new DBPlayer{
			Playername = pPlayername,
			PlayerXP = 0,
			PlayerLevel = 1
		};
		conn.Insert (p);
		return p.Playerid;
	}
	
	public static void updateXPPlayer(int pPlayerID, int pXP, SQLiteConnection conn){
		var p = conn.Table<DBPlayer>().Where(x => x.Playerid == pPlayerID).FirstOrDefault();
		p.PlayerXP = pXP;
		conn.Update(p);
	}
	
	public static void updateLevelPlayer(int pPlayerID, int pLevel, SQLiteConnection conn){
		var p = conn.Table<DBPlayer>().Where(x => x.Playerid == pPlayerID).FirstOrDefault();
		p.PlayerLevel = pLevel;
		conn.Update(p);
	}
	
	public static void updateNamePlayer(int pPlayerID, string pName, SQLiteConnection conn){
		var p = conn.Table<DBPlayer>().Where(x => x.Playerid == pPlayerID).FirstOrDefault();
		p.Playername = pName;
		conn.Update(p);
	}
	
	// public static int insertIntoMonsters(int pPlayerID, int pMonsterLevel,int pMonsterexp, 
										// string pMonsterName, int pMonsterType, SQLiteConnection conn){
	public static int insertIntoMonsters(int pPlayerID, int pMonsterLevel,int pMonsterexp, 
										string pMonsterName, int pMonsterType, int pMonsterHealth,
										int pMonsterMaxHealth, int pMonsterDefenseMod, int pMonsterAttackMod, 
										SQLiteConnection conn){
		
		// var m = new DBMonster{
			// Playerid = pPlayerID,
			// //monsterid = pMonsterID,
			// monsterlevel = pMonsterLevel,
			// monsterexp = pMonsterexp,
			// monstername = pMonsterName,
			// monstertype = pMonsterType
		// };
		
		var m = new DBMonster{
			Playerid = pPlayerID,
			//monsterid = pMonsterID,
			monsterlevel = pMonsterLevel,
			monsterexp = pMonsterexp,
			monstername = pMonsterName,
			monstertype = pMonsterType,
			monsterhealth = pMonsterHealth,
			monstermaxhealth = pMonsterMaxHealth,
			monsterdefensemod = pMonsterDefenseMod,
			monsterattackmod = pMonsterAttackMod
		};
		
		conn.Insert (m);
		
		Debug.Log("Inserting " + m.monstername);
		PrintMonsters();
		return m.monsterid;
		//return p;
	}
	
	public static void updateStatsMonster(int pMonsterID, int pLevel, int pHealth, int pMaxHealth, int pDefMod, int pAtkMod, SQLiteConnection conn){
		var m = conn.Table<DBMonster>().Where(x => x.monsterid == pMonsterID).FirstOrDefault();
		m.monsterlevel = pLevel;
		m.monsterhealth = pHealth;
		m.monstermaxhealth = pMaxHealth;
		m.monsterdefensemod = pDefMod;
		m.monsterattackmod = pAtkMod;
		conn.Update(m);
		Debug.Log("Updated Monster Stats");
		PrintMonsters();
	}
	
	//Need player id to id monster?
	public static void updateXPMonster(int pMonsterID, int pXP, SQLiteConnection conn){
		var m = conn.Table<DBMonster>().Where(x => x.monsterid == pMonsterID).FirstOrDefault();
		m.monsterexp = pXP;
		conn.Update(m);
	}
	
	public static void updateLevelMonster(int pMonsterID, int pLevel, SQLiteConnection conn){
		var m = conn.Table<DBMonster>().Where(x => x.monsterid == pMonsterID).FirstOrDefault();
		m.monsterlevel = pLevel;
		conn.Update(m);
	}
	
	public static void updateNameMonster(int pMonsterID, string pName, SQLiteConnection conn){
		var m = conn.Table<DBMonster>().Where(x => x.monsterid == pMonsterID).FirstOrDefault();
		m.monstername = pName;
		conn.Update(m);
	}
	
	public static void insertIntoInventory(int pPlayerID, int pItemID, SQLiteConnection conn){
		var p = new DBInventory{
			Playerid = pPlayerID,
			itemid = pItemID,
			itemamount = 0
		};
		conn.Insert (p);
	}
	
	public static void decreaseItemInventory(int pItemID, SQLiteConnection conn){
		var p = conn.Table<DBInventory>().Where(x => x.itemid == pItemID).FirstOrDefault();
		
		int amt = p.itemamount;
		if(amt <= 1){
			conn.Delete(p);
		}else{
			amt--;
			p.itemamount = amt;
			conn.Update(p);
		}
	}
	
	public static void increaseItemInventory(int pItemID, SQLiteConnection conn){
		var p = conn.Table<DBInventory>().Where(x => x.itemid == pItemID).FirstOrDefault();
		p.itemamount++;
		conn.Update(p);
	}
	
	
	public static IEnumerable<DBPlayer> GetDBPlayers(SQLiteConnection conn){
		return conn.Table<DBPlayer>();
	}
	
	public static DBMonster SelectDBMonster(int pMonsterID, SQLiteConnection conn){
		var m = conn.Table<DBMonster>().Where(x => x.monsterid == pMonsterID).FirstOrDefault();
		return m;
	}
	
	public static IEnumerable<DBMonster> GetDBMonsters(SQLiteConnection conn){
		return conn.Table<DBMonster>();
	}
	
	public static IEnumerable<DBInventory> GetDBInventory(SQLiteConnection conn){
		return conn.Table<DBInventory>();
	}
	
	public static void DeleteDBPlayer(int pPlayerID, SQLiteConnection conn){
		var p = conn.Table<DBPlayer>().Where(x => x.Playerid == pPlayerID).FirstOrDefault();
		conn.Delete(p);
	}
	
	public static void DeleteDBMonster(int pMonsterID, SQLiteConnection conn){
		var p = conn.Table<DBMonster>().Where(x => x.monsterid == pMonsterID).FirstOrDefault();
		conn.Delete(p);
	}
	
	public static void DeleteDBInventory(int pItemID, SQLiteConnection conn){
		var p = conn.Table<DBInventory>().Where(x => x.itemid == pItemID).FirstOrDefault();
		conn.Delete(p);
	}
	
	public static void saveAs(string filename, SQLiteConnection conn){
		
	}
	
	public static void load(string filename, SQLiteConnection conn){
		
	}
	private static void PrintMonsters(){
		var DBMonsters = DataService.GetDBMonsters(DataService._connection);
		ToConsoleM(DBMonsters);
	}
	
	private static void ToConsoleM(IEnumerable<DBMonster> monsters){
		foreach(var monster in monsters){
			ToConsole(monster.ToString());
		}
	}
	
	private static void PrintPlayers(){
		var dBPlayers = DataService.GetDBPlayers(DataService._connection);
        ToConsole (dBPlayers);
	}
	
	private static void ToConsole(IEnumerable<DBPlayer> players){
		foreach (var player in players) {
			ToConsole(player.ToString());
		}
	}
	
	private static void ToConsole(string msg){
		//DebugText.text += System.Environment.NewLine + msg;
		Debug.Log (msg);
	}
}
