﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class CreateDBScript : MonoBehaviour {

	public Text DebugText;

	// Use this for initialization
	void Start () {
		StartSync();
	}

    private void StartSync()
    {
        var ds = new DataService("tempDatabase.db");
        DataService.CreateDB(DataService._connection);
        
		DataService.insertIntoPlayers("Player 1", DataService._connection);
		DataService.insertIntoPlayers("Player 2", DataService._connection);
		
		
        var dBPlayers = DataService.GetDBPlayers(DataService._connection);
        ToConsole (dBPlayers);
        //dBPlayers = ds.GetPlayersNamedRoberto ();
        //ToConsole("Searching for Roberto ...");
        //ToConsole (dBPlayers); 
    }
	
	private void ToConsole(IEnumerable<DBPlayer> players){
		foreach (var player in players) {
			ToConsole(player.ToString());
		}
	}
	
	private void ToConsole(string msg){
		DebugText.text += System.Environment.NewLine + msg;
		Debug.Log (msg);
	}
}
