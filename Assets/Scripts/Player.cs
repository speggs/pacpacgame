﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Creature {

    public PlayerDetails playerDetails;
    public Monster monsterToFight;
    public GameObject AttackPanel;
    public GameObject ActionPanel;
    private int xp = 0;
    private Button[] attackButtons;
    private Hashtable moveCountTbl = new Hashtable();
	
	private int PlayerID = 0;
	private int MonsterID = 0;
	private string PlayerName;

    public int GetXPRequiredToLevel(int level) {
        return Mathf.CeilToInt(30 *Mathf.Pow(level,1.2f));
    }

    public override void onCreatureKilled() {
        base.onCreatureKilled();
    }

    public void SetNewCreature(string name, int health, int maxhealth, int level, int xp, Moves.Types type) {
        base.SetNewCreature(name, maxhealth, health, level, type);
        playerDetails.SetNewCreature(name, maxhealth, health, level, xp, type);
        this.xp = xp;
		
		// MonsterID = DataService.insertIntoMonsters(PlayerID, level, xp, name, (int)type, DataService._connection);
		if(PlayerPrefs.GetInt("load") == 1){
			MonsterID = PlayerPrefs.GetInt("monsterid");
			PlayerPrefs.SetInt("load", 0);
		}else{
			MonsterID = DataService.insertIntoMonsters(PlayerID, level, xp, name, (int)type, 
												health, maxhealth, defenseMod, attackMod, DataService._connection);
			PlayerPrefs.SetInt("monsterid", MonsterID);
		}
   }

    public void SetHealth(int health)
    {
        Mathf.Clamp(this.health += health,0,this.maxhealth);
        GameManager.manager.onCreatureSetHealth(health);
    }

    public override void DMGAnim(int dmg) {
        base.DMGAnim(dmg);
        playerDetails.SetCreatureHealth(health);
    }

    public override void Start() {
        base.Start();
		PlayerName = "Default Player Name";
		//Create the file and open the connection
		var ds = new DataService("MySave.db");
		//Create tables if DNE
		DataService.CreateDB(DataService._connection);
		PlayerID = DataService.insertIntoPlayers(PlayerName, DataService._connection);
		PlayerPrefs.SetInt("playerid", PlayerID);
    }

    public void onInit(Moves.Types attackType) {
		 if(PlayerPrefs.GetInt("load") == 1){
			int monsterid = PlayerPrefs.GetInt("monsterid");
			DBMonster m = DataService.SelectDBMonster(monsterid, DataService._connection);
			
			int loadHealth = GetHealthByLevel(m.monsterlevel);
			SetNewCreature(m.monstername, loadHealth, loadHealth, m.monsterlevel, m.monsterexp, attackType);
			defenseMod = m.monsterdefensemod;
			attackMod = m.monsterattackmod;
			Debug.Log("Loaded monster named: " + m.monstername);
			//PlayerPrefs.SetInt("load", 0);
		 }else{
			level = 1;
			int health = GetHealthByLevel(GetLevel());
			SetNewCreature(MonsterGenerator.GenerateMonsterName(), health, health, 1, 0, attackType);
		}
    }

    public int GetXP() {
        return xp;
    }

    public int GetXPByKill(int theirLevel) {
        return (int)(GetXPRequiredToLevel(level) / UnityEngine.Random.Range(2f,3f));
        /*int difference = theirLevel - level;

        if (difference >= 0) {
            return level+((difference + level)/2) * UnityEngine.Random.Range(11, 13);
        }
        else {
            return (level/4) * UnityEngine.Random.Range(8,11);
        }*/
    }

    private void SetLevel(int level, int excess) {
        this.level = level;
        GameManager.manager.onPlayerLeveledUp(this,level);
        playerDetails.SetXPPercent(Mathf.Min(1, excess / (float)GetXPRequiredToLevel(level)),excess);
        playerDetails.LevelUpNoVisual();

        int newHealth = GetHealthByLevel(level);
        maxhealth = newHealth;
        health = newHealth;
        playerDetails.SetNewLevelStats(health, maxhealth);
		
		DataService.updateLevelPlayer(PlayerID, level, DataService._connection);
		DataService.updateStatsMonster(MonsterID, level, health, maxhealth, defenseMod, attackMod, DataService._connection);
    }

    public int AddXP(int add) {
        xp += add;
		DataService.updateXPPlayer(PlayerID, xp, DataService._connection);

        int xpToLevel = GetXPRequiredToLevel(level);
        GameManager.manager.onPlayerGainedXP(add,xpToLevel);
        if (xp >= xpToLevel) { // level up
            xp = xp - xpToLevel;
            SetLevel(level + 1,xp);
            return xp;
        }
        else {
			DataService.updateStatsMonster(MonsterID, level, health, maxhealth, defenseMod, attackMod, DataService._connection);
            playerDetails.SetXPPercent(Mathf.Min(1, xp / (float)xpToLevel), add);
            return xp;
        }
    }

    protected override void onCreatureInitialized() {
        base.onCreatureInitialized();
        attackButtons = AttackPanel.GetComponentsInChildren<Button>();

        List<string> moves = new List<string>(moveTbl.Keys);
        
        Player p = this;
        for (int k = 0; k < attackButtons.Length; k++) { // This may have to be worked around if we have more than 1 monster
            string attackname = moves[k];

            int attacks = moveTbl[attackname];
            int index = k;

            Hashtable moveData = new Hashtable();
            moveData["attacksleft"] = moveTbl[attackname];
            moveData["maxattacks"] = moveTbl[attackname];

            moveCountTbl[k] = moveData; // stores min and max for attacks

            Text buttonText = attackButtons[k].GetComponentInChildren<Text>();

            buttonText.text = playerDetails.GetAttackDetails(attackname, attacks, attacks);

            attackButtons[k].onClick.AddListener(delegate {
                Hashtable attackTbl = (Hashtable)moveCountTbl[index];
                //print("index " + index);
                //print(attackTbl["attacksleft"]);
                int movesLeft = (int)attackTbl["attacksleft"];

                if (movesLeft > 0) {
                    p.Attack(attackname, monsterToFight);
                    GameManager.manager.ShowAttacks(false);
                    moveData["attacksleft"] = movesLeft--;
                    playerDetails.UpdateMoveCount(buttonText, attackname, index, movesLeft);
                }
                else {
                    print("no moves left for " + attackname);
                }
            });

        }
        playerDetails.InitializeNewMoves(moveCountTbl);
    }

    public int getMaxHealth()
    {
        return maxhealth;
    }

    public override int onTakeDamage(int dmg) {
        int health = base.onTakeDamage(dmg);
        if (health <= 0) { return 0; }
        return health;
    }

    public void onPlayersTurn() {
    
    }
	
	public void SetName(string name){
		PlayerName = name;
		DataService.updateNamePlayer(PlayerID, name, DataService._connection);
	}
}
