﻿using UnityEngine;

public class HealthKit : Item
{

    public override void OnUse()
    {
        Player player = GameManager.manager.player;
        float newHlth = UnityEngine.Random.Range(.3f, .5f);
        int maxHealth = player.getMaxHealth();
        int hlth = Mathf.CeilToInt(maxHealth * newHlth);

        player.SetHealth(hlth);
    }
}
