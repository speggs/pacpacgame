﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Monster : Creature {

    public MonsterDetails monsterDetails;
    public Player playerObj;
    private bool willDropItem = false;
    public bool isNew = false;

    public void OnHit() {
        this.onTakeDamage(UnityEngine.Random.Range(20, 40));
    }

    public override int onTakeDamage(int dmg) {
        int health = base.onTakeDamage(dmg);
        //EventManager.Call("onMonsterTookDamage");
        if (health <= 0) { return 0; }
        return health;
    }

    public override void onCreatureKilled() {
        base.onCreatureKilled();
        GameManager.manager.onMonsterKilled(this);
    }

    public void SetNewCreature(string name, int health, int maxhealth, int level, Moves.Types type, bool willDropItem) {
        monsterDetails.SetNewCreature(name, maxhealth, health, level, type);
        base.SetNewCreature(name, maxhealth, health, level, type);
        this.willDropItem = willDropItem;
    }

    public override void Start() {
        base.Start();
    }

    public void onInit() {
        level = 1;
        string creatureName = MonsterGenerator.GenerateMonsterName();
        int health = GetHealthByLevel(GetLevel());
        SetNewCreature(creatureName, health, health, 1, Moves.GenerateRandomType(), true);
    }

    public bool WillDropItem() {
        return willDropItem;
    }

    protected override void onCreatureInitialized() {
        base.onCreatureInitialized();
        //MonsterAttack();
    }

   public void MonsterAttack() {

        if (isNew) {
            TextQueue.AddMessageToQueue("The monster has died and cannot attack");
            isNew = false;
            GameManager.manager.onNewEnemyAttack();
            return;
        }

        ArrayList movesWeHave = new ArrayList(moveTbl.Keys);
        ArrayList movesToUse = new ArrayList();

        foreach (string s in movesWeHave) {
            if (moveTbl[s] <= 0) { continue; }
            movesToUse.Add(s);
        }

        System.Random random = new System.Random();

        string moveToUse = (string)movesToUse[random.Next(movesToUse.Count)];

        // Todo actual generation based off different state, this is just random
        this.Attack(moveToUse, playerObj);
    }

    public override void DMGAnim(int dmg) {
        base.DMGAnim(dmg);
        monsterDetails.SetCreatureHealth(health);
    }
}
