﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    private static bool playersTurn = true;
    public Player player;
    public Monster monster;
    public static Scenes scenes;
    public static GameManager manager;
    public CanvasGroup ActionPanel;
    public CanvasGroup AttackPanel;
    public CanvasGroup wholeBottomUI;
    public CanvasGroup starterPicker;
    public CanvasGroup lostScreen;
    public GameObject about2Begin;
    public Button AttackButt;
    public const int numOfAttacks = 4;
    private bool isStarting = false;
    public int score = 0;

    public Button starter1;
    public Button starter2;
    public Button starter3;
    public Button starter4;
    public Button newGameButt;
    public Text scoreTxt;
    public GameObject scoreAnimation;
	
	private int playerID = 0;

    void OnGUI() {
       /* if (GUI.Button(new Rect(10, 5, 150, 50), "Add XP")) {
            player.AddXP(50);
        }*/
    }

    void Start() {
        manager = this;
		
		starterPicker.alpha = 1;
		
        AttackButt.onClick.AddListener(delegate { ShowAttacks(); });

        starter1.onClick.AddListener(delegate { onBattleStarted(Moves.Types.WATER); });
        starter2.onClick.AddListener(delegate { onBattleStarted(Moves.Types.FIRE); });
        starter3.onClick.AddListener(delegate { onBattleStarted(Moves.Types.ELECTRIC); });
        starter4.onClick.AddListener(delegate { onBattleStarted(Moves.Types.NATURE); });

        newGameButt.onClick.AddListener(delegate {
            starterPicker.alpha = 1;
            lostScreen.alpha = 0;
            lostScreen.blocksRaycasts = false;
            starterPicker.blocksRaycasts = true;
        });
		
		if(PlayerPrefs.GetInt("load") == 1){
			//if(PlayerPrefs.HasInt("monsterid")){
				int monsterid = PlayerPrefs.GetInt("monsterid");
				DataService ds = new DataService("MySave.db");
				
				
				DBMonster m = DataService.SelectDBMonster(monsterid, DataService._connection);
				if( ((int)Moves.Types.WATER) == m.monstertype){
					onBattleStarted(Moves.Types.WATER);
				}else if( ((int)Moves.Types.FIRE) == m.monstertype){
					onBattleStarted(Moves.Types.FIRE);
				}else if( ((int)Moves.Types.ELECTRIC) == m.monstertype){
					onBattleStarted(Moves.Types.ELECTRIC);
				}else if( ((int)Moves.Types.NATURE) == m.monstertype){
					onBattleStarted(Moves.Types.NATURE);
				}
				
			//}
		}
    }

    void Update() {
        if (isStarting) {
            about2Begin.GetComponent<Image>().color = new Color(Mathf.PingPong(Time.time * 5, 1), Mathf.PingPong(Time.time * 5, 1), Mathf.PingPong(Time.time * 5, 1), 1f);
        }
    }

    public void onCreatureTookDamage(Creature c,int dmg, int remainingHealth) {
        TextQueue.AddMessageToQueue(c.GetName() + " just got hit for " + dmg.ToString());
    }

    public void onCreatureAttacked(Creature attacker, Creature enemy, string move, Moves.AttackTypes attackType, int val) {
        bool isFriendly = Moves.isMoveFriendly(attackType);
        bool isDmg = Moves.isMoveDmg(attackType);
        bool isDefence = Moves.isMoveDefense(attackType);
        bool isAttack = Moves.isMoveAttack(attackType);

        Hashtable animData = new Hashtable();
        if (isDmg) { // Generic dmg
            animData["target"] = enemy;
            animData["animType"] = Animations.ANIMS.TOOKDMG;
            animData["val"] = val;
        }
        else if(isFriendly && isDefence) { //+def
           // animData["target"] = attacker;
            animData["animType"] = Animations.ANIMS.INCDEF;
        }
        else if(!isFriendly && isDefence) { // -def
            //animData["target"] = enemy;
            animData["animType"] = Animations.ANIMS.DECDEF;
        }
        else if(isFriendly && isAttack) { // +attack
            //animData["target"] = attacker;
            animData["animType"] = Animations.ANIMS.INCDMG;
        }
        else if(!isFriendly && isAttack) { // -attack
            //animData["target"] = enemy;
            animData["animType"] = Animations.ANIMS.DECDMG;
        }

        TextQueue.AddMessageToQueue(attacker.GetName() + " used " + move);
        if (animData.ContainsKey("target")) {
            TextQueue.AddAnimationToQueue(animData); // this is handled elsewhere
        }
    }

    public void onPlayerKilled() {
        TextQueue.ClearTextQueue();
        scoreAnimation.SetActive(true);
        lostScreen.alpha = 1;
        lostScreen.blocksRaycasts = true;
        scoreTxt.text = string.Format(scoreTxt.text, score);
        scoreAnimation.GetComponent<Animator>().Play("PlayerLost");
    }

    public void onMonsterTookDamage(int dmg) {

    }

    public void onCreatureSetHealth(int health)
    {
        TextQueue.AddMessageToQueue(player.GetName() + " got " + health + " health");
    }

    public void onMonsterKilled(Monster m) {
        playersTurn = true;
        onTurnChange(playersTurn);
        int health = m.GetHealthByLevel(player.GetLevel());
        int xpPlayerEarns = player.GetXPByKill(player.GetLevel());
        player.AddXP(xpPlayerEarns);
        m.SetNewCreature(MonsterGenerator.GenerateMonsterName(), health, health, player.GetLevel(), Moves.GenerateRandomType(), true);
        m.isNew = true;
        score++;
		switch (score % 3){
			case 0:
				HealthKit hk = gameObject.AddComponent<HealthKit>();
				hk.OnUse();
                Destroy(gameObject.GetComponent<HealthKit>());
                TextQueue.AddMessageToQueue("You got a Health Kit for killing a monster!");
                break;
			case 1:
				AttackPotion ap =  gameObject.AddComponent<AttackPotion>();
				ap.OnUse();
                Destroy(gameObject.GetComponent<AttackPotion>());
                TextQueue.AddMessageToQueue("You got a Attack Potion for killing a monster!");
                break;
			case 2:
				DefensePotion dp = gameObject.AddComponent<DefensePotion>();
				dp.OnUse();
                Destroy(gameObject.GetComponent<DefensePotion>());
                TextQueue.AddMessageToQueue("You got a Defense Potion for killing a monster!");
                break;
		}
    }

    public void onPlayerRan(bool ranAway) {
        if (ranAway) {
            TextQueue.AddMessageToQueue("You ran away!");
        }
        else {
            TextQueue.AddMessageToQueue("You failed to run away!");
        }
    }

    public void onCreatureKilled(Creature c) {
        TextQueue.AddMessageToQueue(c.GetName() + " has fainted!");
    }

    private IEnumerator RemoveScreens(Moves.Types typePicked) {
        yield return new WaitForSeconds(3);
        about2Begin.GetComponent<CanvasGroup>().blocksRaycasts = false;
        about2Begin.GetComponent<CanvasGroup>().alpha = 0f;
        about2Begin.SetActive(false);
        player.onInit(typePicked);
        monster.onInit();
    }

    public void onBattleStarted(Moves.Types typePicked) {
        AudioClip clip = (AudioClip)Resources.Load("Sounds/PacBattle");
        GetComponent<AudioSource>().clip = clip;
        GetComponent<AudioSource>().Play();
        starterPicker.alpha = 0;
        starterPicker.blocksRaycasts = false;
        isStarting = true;

        about2Begin.SetActive(true);
        about2Begin.GetComponent<Animator>().Play("BattleStart");
        about2Begin.GetComponent<CanvasGroup>().blocksRaycasts = true;
        about2Begin.GetComponent<CanvasGroup>().alpha = 1f;

        StartCoroutine(RemoveScreens(typePicked));
    }

    public void onPlayerLeveledUp(Player p, int level) {
        TextQueue.AddMessageToQueue(p.GetName() + " hit level " + level);
        p.HealthAnim();
    }

    public void onPlayerGainedXP(int xp, int xpNeededToLevel) {

    }

    public void onTurnChange(bool playersTurn) {
        if (!playersTurn) {
            if (monster.GetHealth() <= 0) {
                TextQueue.AddMessageToQueue("The monster has died and cannot attack");
                playersTurn = true;
                this.onTurnChange(playersTurn);
                return;
            }
            monster.MonsterAttack();
            ActionPanel.alpha = 0f;
            AttackPanel.alpha = 0f;
        }
        else {
            ActionPanel.alpha = 1f;
            AttackPanel.alpha = 0f;
        }
    }

    public void onPlayerAttacked(string attack, int dmg) {
        playersTurn = false;
        this.onTurnChange(playersTurn);
    }

    public void onEnemyAttacked(string attack, int dmg) {
        playersTurn = true;
        this.onTurnChange(playersTurn);
    }

    public void onNewEnemyAttack() {
        playersTurn = true;
        this.onTurnChange(playersTurn);
    }

    public void onCreatureDefenseChanged(Creature c, int change) {

        Hashtable animData = new Hashtable();
        animData["target"] = c;

        if (change > 0) {
            TextQueue.AddMessageToQueue(c.GetName() + " has gained defense!");
            animData["animType"] = Animations.ANIMS.INCDEF;
        }
        else {
            TextQueue.AddMessageToQueue(c.GetName() + " has lost defense!");
            animData["animType"] = Animations.ANIMS.DECDEF;
        }

        TextQueue.AddAnimationToQueue(animData);

    }

    public void onCreatureAttackChanged(Creature c, int change) {
        Hashtable animData = new Hashtable();
        animData["target"] = c;
        if (change > 0) {
            TextQueue.AddMessageToQueue(c.GetName() + " has gained attack damage!");
            animData["animType"] = Animations.ANIMS.INCDMG;
        }
        else {
            TextQueue.AddMessageToQueue(c.GetName() + " has lost attack damage!");
            animData["animType"] = Animations.ANIMS.DECDMG;
        }
        TextQueue.AddAnimationToQueue(animData);
    }

    public static bool isPlayersTurn() {
        return playersTurn;
    }

    public void ShowAttacks(bool b = true) {
        if (b) {
            AttackPanel.alpha = 1f;
            ActionPanel.alpha = 0f;

            ActionPanel.blocksRaycasts = false;
            AttackPanel.blocksRaycasts = true;
        }
        else {
            AttackPanel.alpha = 0f;
            ActionPanel.alpha = 1f;

            ActionPanel.blocksRaycasts = true;
            AttackPanel.blocksRaycasts = false;
        }
    }
}
