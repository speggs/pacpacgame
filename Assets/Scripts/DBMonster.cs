using SQLite4Unity3d;

public class DBMonster  {
	
	[PrimaryKey, AutoIncrement]
	public int monsterid { get; set; }
	public int Playerid { get; set; }
	public int monsterlevel { get; set; }
	public int monsterexp { get; set; }
	public string monstername { get; set; }
	public int monstertype { get; set; }
	public int monsterhealth { get; set; }
	public int monstermaxhealth { get; set; }
	public int monsterdefensemod { get; set; }
	public int monsterattackmod { get; set; }

	public override string ToString ()
	{
		// return string.Format ("[DBMonster: Playerid={0}, monsterid={1}, monsterlevel={2}, monsterexp={3}, monstername={4}, monstertype={5}]", Playerid, monsterid, monsterlevel, monsterexp, monstername, monstertype);
		return string.Format ("[DBMonster: Playerid={0}, monsterid={1}, monsterlevel={2}, monsterexp={3}, monstername={4}, monstertype={5}, monsterhealth={6}, monstermaxhealth={7}, monsterdefensemod={8}, monsterattmod={9}]", Playerid, monsterid, monsterlevel, monsterexp, monstername, monstertype, monsterhealth, monstermaxhealth, monsterdefensemod, monsterattackmod);
	}
}